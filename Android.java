public class Android
{
	public double AmountOfMemory;
	public String Model;
	public String AndroidVersion;
	
	public void MemoryCheck(double Memory)
	{
		double TrueMemory = (Memory - 2.3);
		System.out.println("With the OS of the Android phone taking up around 2.3GBs, the true amount of memory that the phone has is " + TrueMemory + ". We are sorry for the confusion.");
	}
}