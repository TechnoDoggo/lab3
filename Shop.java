public class Shop
{
	public static void main(String[] args)
	{
		java.util.Scanner reader = new
		java.util.Scanner(System.in);
		
		Android Phones = new Android();
		Android TrueMemory = new Android();
		
		Android[] products = new Android[4];
		
		for (int i = 0; i < 4; i++)
		{
			System.out.println("Current Product: Android Phone #" + (i + 1));
			
			System.out.println("Please input the memory of the Android Phone in Gigabites (GBs).");
			String MemoryString = reader.nextLine();
			Phones.AmountOfMemory = Double.parseDouble(MemoryString);
			
			System.out.println("Please input the model of the Android Phone.");
			Phones.Model = reader.nextLine();
			
			System.out.println("Please input the current version of Android that is currently running on the Android Phone.");
			Phones.AndroidVersion = reader.nextLine();
		}
		
		System.out.println("Info on 4th Android Phone.");
		System.out.println("The memory of the Android Phone is : " + Phones.AmountOfMemory + " GBs");
		System.out.println("The model of the Android Phone is : " + Phones.Model);
		System.out.println("The current version of the Android Phone is : " + Phones.AndroidVersion);
		
		TrueMemory.MemoryCheck(Phones.AmountOfMemory);
	}
}